public struct Board: CustomStringConvertible {
    
    
    
    
    
    
    public let nbRows:Int
    public let nbColumns:Int
    public var grid:[[Int?]]
    
    public var isPlayer1Turn:Bool = true
    
    
    private static let descriptionMapper : [Int? : String] = [nil : " ", 1 : "X", 2 : "O"]
    
    public var description: String {
        var string = String()
        for row in grid.reversed(){
            for cell in row {
                string.append("\(String(describing: Board.descriptionMapper[cell] ?? "-")) ")
            }
            string.append("\n")
        }
        return string
    }
    
    public init?(nbR : Int = 10, nbC : Int = 10) {
        guard nbR > 0 && nbC > 0 else {
            return nil;
        }
        self.nbRows = nbR;
        self.nbColumns = nbC;
        grid = Array(repeating: Array(repeating: nil, count: nbColumns), count:nbRows);
    }
    
    public mutating func insertCoin(col:Int)->Bool{
        let coll = col - 1
        guard (0...self.nbColumns-1).contains(coll) else {
            print("col out of range")
            return false;
            
        }
        for row in 0..<nbRows{
            if(grid[row][coll]==nil){
                self.grid[row][coll] = ( isPlayer1Turn ? 1 : 2 );
                isPlayer1Turn = !isPlayer1Turn
                return true
            }
        }
        print("row is full")
        return false
    }
    
    public func showBoard()->Void{
        for row in (0 ..< nbRows).reversed(){
            print(row+1," ", terminator: "")
            for cell in 0..<nbColumns {
                print("| ", terminator: "")
                print(Board.descriptionMapper[grid[row][cell]] ?? "M", terminator: " ")
                
            }
            print("|", terminator: "")
            print("\n")
        }
        print("     ", terminator: "")
        for cell in 0..<nbColumns {
            print(cell+1,terminator: "   ")
        }
        print("\n")
    }
}
