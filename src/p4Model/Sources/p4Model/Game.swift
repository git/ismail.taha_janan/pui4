//
//  File.swift
//  
//
//  Created by etudiant on 31/01/2023.
//

import Foundation

public class Game{
    public var boa:Board
    public var isGameContinuing:Bool = true
    var choosedCol:Int?

    var gameLoop:Bool = true
    var isPlayer1:Bool = true;
    let withIA:Bool
    
    let rules:GameRules
    
    
    public init(withColNum numCol:Int, andRowNum rowNum:Int, andPlayWithIA withIA:Bool) {
        self.boa = Board(nbR: numCol,nbC: rowNum)!
        self.withIA = withIA
        rules = GameRules(boa: self.boa)
    }
    
    
    public func start()->Void{
        while(gameLoop){
            
            boa.showBoard()
            
            print((boa.isPlayer1Turn ? "Player 1 turn" : "Player 2 turn"))
            if (withIA && !isPlayer1) {
          
                boa.insertCoin(col: Int.random(in: 0...boa.nbColumns))
                isPlayer1 = !isPlayer1
                
            } else {
                
                if let typed = readLine() {
                  if let num = Int(typed) {
                      
                      boa.insertCoin(col: num)
                      isPlayer1 = !isPlayer1
                      
                  }else {print("wrong Input")}
                }
            }
            
            if(isPlayer1){
                if(rules.checkWin(player: 1)){
                    print("player 1 win")
                    gameLoop = false
                }
            }else if(!isPlayer1){
                if(rules.checkWin(player: 2)){
                    print("player 2 win")
                    gameLoop = false
                }
                
            }
        }
    }
}
