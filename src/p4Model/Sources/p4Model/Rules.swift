//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//

import Foundation

public class GameRules{
    
    private var boa:Board
    private let nbRows:Int
    private let nbCels:Int
    
    public init(boa:Board){
        self.boa=boa
        self.nbRows=boa.nbRows
        self.nbCels=boa.nbColumns
    }
    

    public func checkWin(player: Int) -> Bool {
        // Check horizontal
        for row in 0..<nbRows {
            for col in 0..<nbCels-3 {
                if boa.grid[row][col] == player &&
                    boa.grid[row][col+1] == player &&
                    boa.grid[row][col+2] == player &&
                    boa.grid[row][col+3] == player {
                    return true
                }
            }
        }

        // Check vertical
        for row in 0..<nbRows-3 {
            for col in 0..<nbCels {
                if boa.grid[row][col] == player &&
                    boa.grid[row+1][col] == player &&
                    boa.grid[row+2][col] == player &&
                    boa.grid[row+3][col] == player {
                    return true
                }
            }
        }

        // Check ascending diagonal
        for row in 3..<nbRows{
            for col in 0..<nbCels-3 {
                if boa.grid[row][col] == player &&
                    boa.grid[row-1][col+1] == player &&
                    boa.grid[row-2][col+2] == player &&
                    boa.grid[row-3][col+3] == player {
                    return true
                }
            }
        }

        // Check descending diagonal
        for row in 0..<nbRows-3 {
            for col in 0..<nbCels-3 {
                if boa.grid[row][col] == player &&
                    boa.grid[row+1][col+1] == player &&
                    boa.grid[row+2][col+2] == player &&
                    boa.grid[row+3][col+3] == player {
                    return true
                }
            }
        }

        return false
    }
    
    
    
}
